import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import Modal from './modal';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.checkedFighters = [];
    this.check = this.onFighterCheck.bind(this);
    this.update = this.onFighterChanged.bind(this);
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  fight(firstFighter, secondFighter) {
    let firstHit;
    let firstBlock;
    let secondHit;
    let secondBlock;
    while (firstFighter.health > 0 && secondFighter.health > 0) {
      firstHit = firstFighter.getHitPower();
      firstBlock = firstFighter.getBlockPower();
      secondHit = secondFighter.getHitPower();
      secondBlock = secondFighter.getBlockPower();

      firstFighter.health = firstFighter.health - (secondHit <= firstBlock ? 0 : secondHit - firstBlock);
      secondFighter.health = secondFighter.health - (firstHit <= secondBlock ? 0 :  firstHit - secondBlock);
      if(firstFighter.health <= 0) {
        alert(`${secondFighter.name} WINNER`)
      } else if(secondFighter.health <= 0) {
        alert(`${firstFighter.name} WINNER`)
      }
    }
    const firstBackground = document.getElementsByClassName('fighter-image')[firstFighter.id-1];
    const secondBackground = document.getElementsByClassName('fighter-image')[secondFighter.id-1];
    firstBackground.style.backgroundColor = '';
    secondBackground.style.backgroundColor = '';
    this.checkedFighters = [];
    const forRemove = document.getElementById('start-fight');
    forRemove.remove();
  }

  onFighterChanged(id) {
    const fighter = this.fightersDetailsMap.get(id);
    const {health, attack, defense} = fighter;
    const healthValue = document.getElementsByClassName('input-health')[0].value;
    const attackValue = document.getElementsByClassName('input-attack')[0].value;
    const defenseValue = document.getElementsByClassName('input-defense')[0].value;
    if(+healthValue !== health || +attackValue !== attack || +defenseValue !== defense) {
      fighter.health = +healthValue;
      fighter.attack = +attackValue;
      fighter.defense = +defenseValue;
      this.fightersDetailsMap.set(id, fighter);
      alert('Fighter is updated');
    } else alert('You haven\'t changes' );
  }

  readyToFight(fighterLength) {
    if(fighterLength === 2) {
      const root = document.getElementById('root');
      const btn = this.createElement({tagName: 'button', className: 'start-fight', attributes: {id: 'start-fight'}});
      btn.innerText = 'Fight';
      btn.style.cssText = "width: 200px; height: 50px; margin-bottom: 30px";
      btn.onclick = () => this.fight(this.checkedFighters[0], this.checkedFighters[1]);
      root.append(btn);
    }
  }

  onFighterCheck(id){
    const imageBackground = document.getElementsByClassName('fighter-image')[id-1];
    imageBackground.style.backgroundColor = 'lightgreen';
    const span = document.getElementsByClassName("close")[0];
    if (this.checkedFighters.length < 2) {
      const {_id, name, health, attack, defense} = this.fightersDetailsMap.get(id);
      for(let i of this.checkedFighters) {
        if (i.id === id) {
          alert('You can\'t add the same fighter');
          return
        }
      }
      this.checkedFighters.push(new Fighter(_id, name, health, attack, defense));
      this.readyToFight(this.checkedFighters.length);
      alert('Fighter added');
      span.click();
    } else alert('Only two fighters can take part at the same time');
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    const {_id} = fighter;
    if(!this.fightersDetailsMap.has(_id)){
      this.fightersDetailsMap.set(_id, fighter);
    }
    if (this.fightersDetailsMap.has(_id) && !this.fightersDetailsMap.get(_id).health) {
      fighterService.getFighterDetails(_id)
        .then(res => {
          this.fightersDetailsMap.set(_id, res);
          new Modal(this.fightersDetailsMap.get(_id), this.check, this.update);
          return this.fightersDetailsMap.get(_id);
        })
        .catch(err => console.error(err))
    } else {
      new Modal(this.fightersDetailsMap.get(_id), this.check, this.update);
    }
  }
}

export default FightersView;
