import View from './view';

class Modal extends View {
  constructor(fighterDetails, checkFighter, updateFighter) {
    super();

    this.checkFighter = checkFighter;
    this.updateFighter = updateFighter;
    this.showModal(fighterDetails);
  }

  showModal(fighterDetails) {
    const {_id, name, health, attack, defense} = fighterDetails;
    const modal = document.getElementById("modalDetails");
    const span = document.getElementsByClassName("close")[0];
    const details = document.getElementsByClassName('details')[0];
    details.innerHTML = '';
    const detailsHeader = this.createElement({tagName: 'div', className: 'details-header'});
    const detailsBody = this.createElement({tagName: 'div', className: 'details-body'});
    const detailsBodyHealth = this.createElement({tagName: 'div', className: 'health'});
    const detailsBodyAttack = this.createElement({tagName: 'div', className: 'attack'});
    const detailsBodyDefense = this.createElement({tagName: 'div', className: 'defense'});
    const detailsFooter = this.createElement({tagName: 'div', className: 'details-footer'});
    const buttonCheck = this.createElement({tagName: 'button', className: 'btn-check'});
    const buttonUpdate = this.createElement({tagName: 'button', className: 'btn-update'});
    buttonCheck.innerText = 'Check Fighter';
    buttonUpdate.innerText = 'Update Fighter';

    const fighterName = this.createElement({tagName: 'p', className: 'fighter-name'});
    fighterName.innerText = `Name: ${name} `;
    detailsHeader.appendChild(fighterName);

    const labelHealth = this.createElement({tagName: 'label', className: 'label-health', attributes: {for: "input-health"}});
    labelHealth.innerText = "Health: ";
    const inputHealth = this.createElement({tagName: 'input', className: 'input-health', attributes: {value: health}});
    detailsBodyHealth.appendChild(labelHealth);
    detailsBodyHealth.appendChild(inputHealth);
    detailsBody.appendChild(detailsBodyHealth);

    const labelAttack = this.createElement({tagName: 'label', className: 'label-attack', attributes: {for: "input-attack"}});
    labelAttack.innerText = "Attack: ";
    const inputAttack = this.createElement({tagName: 'input', className: 'input-attack', attributes: {value: attack}});
    detailsBodyAttack.appendChild(labelAttack);
    detailsBodyAttack.appendChild(inputAttack);
    detailsBody.appendChild(detailsBodyAttack);

    const labelDefense = this.createElement({tagName: 'label', className: 'label-defense', attributes: {for: "input-defense"}});
    labelDefense.innerText = "Defense: ";
    const inputDefense = this.createElement({tagName: 'input', className: 'input-defense', attributes: {value: defense}});
    detailsBodyDefense.appendChild(labelDefense);
    detailsBodyDefense.appendChild(inputDefense);
    detailsBody.appendChild(detailsBodyDefense);

    detailsFooter.appendChild(buttonCheck);
    detailsFooter.appendChild(buttonUpdate);

    details.appendChild(detailsHeader);
    details.appendChild(detailsBody);
    details.appendChild(detailsFooter);

    modal.style.display = "block";
    span.onclick = function () {
      modal.style.display = "none";
    };

    buttonCheck.onclick = () => this.checkFighter(_id);
    buttonUpdate.onclick = () => this.updateFighter(_id);
    window.onclick = function (event) {
      if (event.target === modal) {
        modal.style.display = "none";
      }
    };
  }
}

export default Modal;

